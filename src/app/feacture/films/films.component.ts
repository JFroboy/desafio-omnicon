import { Component, OnInit } from '@angular/core';

/** Servicios */
import { FilmServices } from '../../shared/services/film.services';
import { CharacterServices } from '../../shared/services/characters.services';
import { SweetAlertsService } from '../../shared/services/sweet-alets.services';

/** Modelos */
import { Film } from '../../shared/models/film';
import { Character} from '../../shared/models/Character';

@Component({
  selector: 'app-films',
  templateUrl: './films.component.html',
  styleUrls: ['./films.component.css']
})
export class FilmsComponent implements OnInit {
  listFilms: Film[];
  filmSeleccionado: Film;
  listPersonajes: Character[] = [];

  infomarcionDetallada: boolean = false;

  constructor(
    private filmServices: FilmServices,
    private characterServices: CharacterServices,
    private sweetAlertsService: SweetAlertsService
  ) { 
    this.consultar();
  }

  ngOnInit(): void {
  }

  consultar(){
    this.filmServices.getFilms().subscribe((data: any) => {
      this.listFilms = data.results;
    })
  }

  detallesPersonajes(film: any){
    this.filmSeleccionado = film;
    this.filmSeleccionado.characters.forEach((element: any) => {
      this.characterServices.getCharacter(element).subscribe((character: any) => {
        this.listPersonajes.push(character);
      })
    });
    this.infomarcionDetallada = true;
  }

  informacionNave(film: any){
    console.log(film)
  }
}
