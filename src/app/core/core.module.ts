import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { LoaderComponent } from './loader/loader.component';

import { AppRoutingModule } from '../app-routing.module';
import { FooterComponent } from './footer/footer.component';
import { NavbarComponent } from './navbar/navbar.component';


@NgModule({
  declarations: [
    LoaderComponent,
    FooterComponent,
    NavbarComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule
  ],
  exports: [
      LoaderComponent,
      NavbarComponent
  ],
})
export class CoreModule { }
