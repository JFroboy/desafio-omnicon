import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FilmsComponent } from './feacture/films/films.component'

const routes: Routes = [
  { path: 'films', component: FilmsComponent },
  { path: '**', redirectTo: '/films', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
