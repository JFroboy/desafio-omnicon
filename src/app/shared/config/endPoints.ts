import { environment } from 'src/environments/environment';
import { HttpHeaders } from '@angular/common/http';

const { URL } = environment;

export const apiFilms = `${URL}/films`
export const apiCharacters = `${URL}/people`

export const httpOptions = {
    headers: new HttpHeaders({
    })
};
