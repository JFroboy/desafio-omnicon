export class Film {
    constructor(
      public title: string,
      public episode_id: string,
      public opening_crawl: string,
      public director: string,
      public producer: string,
      public release_date: string,
      public characters: any[],
      public planets: any[],
      public starships: any[],
      public vehicles: any[],
      public species: any[],
      public edited: string,
      public created: string,
      public url: string,
    ) {}
}