export class Character {
    constructor(
      public name: string,
      public height: string,
      public mass: string,
      public hair_color: string,
      public skin_color: string,
      public eye_color: string,
      public birth_year: string,
      public gender: string,
      public homeworld: string,
      public films: any[],
      public vehicles: any[],
      public species: any[],
      public starships: any[],
      public edited: string,
      public created: string,
      public url: string,
    ) {}
}