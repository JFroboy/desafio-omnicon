import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { httpOptions} from '../config/endPoints';

@Injectable({
  providedIn: 'root'
})
export class StarShipServices {

  constructor(
    private http: HttpClient) {
  }

  /**
   * Se obtiene la información de cada nave, la ulr la provee uno de los atributos de los personajes (Character)
   */
  getStarShip(url: string) {
    return this.http.get(url, httpOptions).pipe();
  }
}