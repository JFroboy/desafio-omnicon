import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { httpOptions, apiFilms} from '../config/endPoints';

@Injectable({
  providedIn: 'root'
})
export class FilmServices {

  constructor(
    private http: HttpClient) {
  }

  /**
   * Se obtienen las peliculas de Star Wars almacenadas en la base de datos de Swapi
   */
  getFilms() {
    return this.http.get(apiFilms, httpOptions).pipe();
  }
}
