import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { httpOptions, apiCharacters} from '../config/endPoints';

@Injectable({
  providedIn: 'root'
})
export class CharacterServices {

  constructor(
    private http: HttpClient) {
  }

  /**
   * Se obtiene la información de cada personajes, la ulr la provee uno de los atributos de la pelicula
   */
  getCharacter(url: string) {
    return this.http.get(url, httpOptions).pipe();
  }
}
