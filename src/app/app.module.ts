import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { CoreModule } from './core/core.module';

import { LoaderComponent } from './core/loader/loader.component';
import { LoaderInterceptorService } from './core/loader/loader-interceptor.service';
import { FilmsComponent } from './feacture/films/films.component';


@NgModule({
  declarations: [
    AppComponent,
    FilmsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    CoreModule,
  ],
  providers: [
    [{
      provide: HTTP_INTERCEPTORS,
      useClass: LoaderInterceptorService,
      multi: true
    }]
  ],
  entryComponents: [LoaderComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
